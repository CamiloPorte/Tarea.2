class Nodo
	attr_accessor :value, :left, :right, :altura
	def initialize (value)
		@value = value
		@left = nil
		@right = nil
		@altura = 0
	end

	def log2(x)
		Math.log(x)/Math.log(2)
	end

	def calcularAltura
		return log2(altura+1)
	end

	def equilibrioCaso1R
		aux  = Nodo
		aux.altura = aux.altura - aux.right.altura
		temp = Nodo.right.left
		@Nodo = Nodo.right
		Nodo.left = aux
		Nodo.left.right = temp
		@Nodo.altura = @Nodo.altura + aux.altura
		aux.altura = aux.altura + temp.altura
	end

	def EquilibrioCaso2R
		aux  = Nodo.right
		aux.altura = aux.altura - Nodo.left.right.right.altura
		temp = Nodo.right.left.right
		Nodo.right = Nodo.right.left
		Nodo.right.right = aux
		Nodo.right.right.left = temp
		Nodo.right.altura = Nodo.right.altura + aux.altura - temp.altura
		Nodo.EquilibrioCaso1R
	end

	def equilibrioCaso1L
		aux   = Nodo
		aux.altura = aux.altura - aux.left.altura
		temp  = Nodo.left.right
		@Nodo = Nodo.left
		Nodo.right = aux
		Nodo.right.left = temp
		@Nodo.altura = @Nodo.altura + aux.altura
		aux.altura = aux.altura + temp.altura
	end

	def equilibrioCaso2L
		aux  = Nodo.left
		aux.altura =aux.altura - Nodo.right.left.left.altura
		temp = Nodo.left.right.left
		Nodo.left = Nodo.left.right
		Nodo.left.left = aux
		Nodo.left.left.right = temp
		Nodo.left.altura = Nodo.left.altura + aux.altura - temp.altura
		Nodo.EquilibrioCaso1L
	end

	def Equilibrio
		if(Nodo.left.calcularAltura - Nodo.right.calcularAltura) > 1
			if (Nodo.left.left.calcularAltura - Nodo.left.right.calcularAltura) < -1
				Nodo.equilibrioCaso2L
			else
				Nodo.equilibrioCaso1L
			end
		end
		if(Nodo.right.calcularAltura - Nodo.left.calcularAltura) > 1
			if (Nodo.right.right.calcularAltura - Nodo.right.left.calcularAltura) < -1
				Nodo.EquilibrioCaso2R
			else
				Nodo.EquilibrioCaso1R
			end
		end
		return
	end
end

class Avl
	attr_accessor :root
	def initialize
		@root = nil
	end

	def empty
		if root == nil 
			return true
		else
			return false
		end
	end

	def insertar(nodoValue)
		if empty == true
			root == Nodo.New(nodoValue)
		else
			aux = @root
			while true
				if nodoValue < aux.value
					if aux.left != nil
						aux.altura = aux.altura + 1
						aux = aux.left
					else
						aux.altura = aux.altura + 1
						aux.left = Nodo.New(nodoValue)
						root.Equilibrio
						return
					end
				end
				if nodoValue > aux.value 
					if aux.right != nil
						aux.altura = aux.altura + 1
						aux = aux.right
					else 
						aux.altura = aux.altura + 1
						aux.right = Nodo.New(nodoValue)
						root.Equilibrio
						return
					end
				end
			end
		end
	end
end