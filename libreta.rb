class Nodo
	attr_accessor :direccion, :nombre,:apellido, :left, :right, :altura,:next,:telefono,:email,:value
	def initialize (direccion, nombre, apellido,telefono,email)
		@direccion = direccion
		@nombre = nombre
		@apellido = apellido
		@left = nil
		@right = nil
		@altura = 0
		@next = nil
		@telefono = telefono
		@email= email
		@value = direccion.ord + nombre.ord + email.ord + telefono
	end
	def log2(x)
		Math.log(x)/Math.log(2)
	end

	def calcularAltura
		return log2(altura+1)
	end

	def equilibrioCaso1R
		aux  = Nodo
		aux.altura = aux.altura - aux.right.altura
		temp = Nodo.right.left
		@Nodo = Nodo.right
		Nodo.left = aux
		Nodo.left.right = temp
		@Nodo.altura = @Nodo.altura + aux.altura
		aux.altura = aux.altura + temp.altura
	end

	def EquilibrioCaso2R
		aux  = Nodo.right
		aux.altura = aux.altura - Nodo.left.right.right.altura
		temp = Nodo.right.left.right
		Nodo.right = Nodo.right.left
		Nodo.right.right = aux
		Nodo.right.right.left = temp
		Nodo.right.altura = Nodo.right.altura + aux.altura - temp.altura
		Nodo.EquilibrioCaso1R
	end

	def equilibrioCaso1L
		aux   = Nodo
		aux.altura = aux.altura - aux.left.altura
		temp  = Nodo.left.right
		@Nodo = Nodo.left
		Nodo.right = aux
		Nodo.right.left = temp
		@Nodo.altura = @Nodo.altura + aux.altura
		aux.altura = aux.altura + temp.altura
	end

	def equilibrioCaso2L
		aux  = Nodo.left
		aux.altura =aux.altura - Nodo.right.left.left.altura
		temp = Nodo.left.right.left
		Nodo.left = Nodo.left.right
		Nodo.left.left = aux
		Nodo.left.left.right = temp
		Nodo.left.altura = Nodo.left.altura + aux.altura - temp.altura
		Nodo.EquilibrioCaso1L
	end

	def Equilibrio
		if(Nodo.left.calcularAltura - Nodo.right.calcularAltura) > 1
			if (Nodo.left.left.calcularAltura - Nodo.left.right.calcularAltura) < -1
				Nodo.equilibrioCaso2L
			else
				Nodo.equilibrioCaso1L
			end
		end
		if(Nodo.right.calcularAltura - Nodo.left.calcularAltura) > 1
			if (Nodo.right.right.calcularAltura - Nodo.right.left.calcularAltura) < -1
				Nodo.EquilibrioCaso2R
			else
				Nodo.EquilibrioCaso1R
			end
		end
		return
	end
end

class Avl
	attr_accessor :root
	def initialize
		@root = nil
	end

	def empty
		if root == nil 
			return true
		else
			return false
		end
	end

	def insertar(nodoValue)
		if empty == true
			root == Nodo.New(nodoValue)
		else
			aux = @root
			while true
				if nodoValue < aux.value
					if aux.left != nil
						aux.altura = aux.altura + 1
						aux = aux.left
					else
						aux.altura = aux.altura + 1
						aux.left = Nodo.New(nodoValue)
						root.Equilibrio
						return
					end
				end
				if nodoValue > aux.value 
					if aux.right != nil
						aux.altura = aux.altura + 1
						aux = aux.right
					else 
						aux.altura = aux.altura + 1
						aux.right = Nodo.New(nodoValue)
						root.Equilibrio
						return
					end
				end
			end
		end
	end
end

class HASH
	attr_accessor :tabla_hash
	def initialize()
		@tabla_hash = Array.new(50)
	end

	def funcionHash(value)
		return  (((21474 * 131071) ^ (524287*value))%50)
	end

	def insertar(n)
		posicion = funcionHash(n)
		if @tabla_hash[posicion] == nil
			@tabla_hash[posicion] = Nodo.new(n)
			puts "se inserto : #{@tabla_hash[posicion].value}"
		else
			actual = @tabla_hash[posicion]
			until actual.next == nil
				actual = actual.next
			end
			actual.next = Nodo.new(n)
			puts "se inserto #{actual.next.value}"
		end
	end

	def buscar(valor)
		posicion = funcionHash(valor)
		if @tabla_hash[posicion]== nil
			puts"#{valor} no encontrado"
			return
		end
		if @tabla_hash[posicion].value == valor
			puts "#{valor} encontrado"
			return @tabla_hash[posicion]
		else
			actual = @tabla_hash[posicion]
			until actual == nil
				if actual.value == valor
					puts "#{valor} encontrado"
					return actual
				end
				actual = actual.next
			end
			puts "#{valor} no encontrado"
			return
		end
	end

	def eliminar(value)
		posicion = funcionHash(value)
		if @tabla_hash[posicion] == nil
			puts "#{value} no encontrado"
			return
		end
		if @tabla_hash[posicion].value == value and @tabla_hash[posicion].next == nil 
			@tabla_hash[posicion] = nil
			puts "#{value} fue eliminado"
			return
		else
			if @tabla_hash[posicion].value == value
				@tabla_hash[posicion] = @tabla_hash[posicion].next
				puts "#{value} fue eliminado"
				return
			end
			actual = @tabla_hash[posicion]
			until actual.next == nil
				if actual.next.value == value
					actual.next = actual.next.next
					puts "#{value} fue eliminado"
				end
				actual = actual.next
			end
			puts "#{valor} no encontrado"
			return
		end
	end

	def dibujar
		posicion = 0
		while posicion != 49
			if @tabla_hash[posicion] != nil
				puts @tabla_hash[posicion].value
				if @tabla_hash[posicion].next != nil
					aux = @tabla_hash[posicion]
					while aux.next != nil
						puts "#{posicion}.- #{aux.next.value}"
						aux = aux.next
					end
				end
				posicion = posicion + 1
			else
				posicion = posicion + 1
			end
		end
	end
end

