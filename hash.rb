class Nodo
	attr_accessor :value, :next
	def initialize (value)
		@value = value
		@next = nil
	end
end

class HASH
	attr_accessor :tabla_hash
	def initialize()
		@tabla_hash = Array.new(50)
	end

	def funcionHash(value)
		return  (((21474 * 131071) ^ (524287*value))%50)
	end

	def insertar(n)
		posicion = funcionHash(n)
		if @tabla_hash[posicion] == nil
			@tabla_hash[posicion] = Nodo.new(n)
			puts "se inserto : #{@tabla_hash[posicion].value}"
		else
			actual = @tabla_hash[posicion]
			until actual.next == nil
				actual = actual.next
			end
			actual.next = Nodo.new(n)
			puts "se inserto #{actual.next.value}"
		end
	end

	def buscar(valor)
		posicion = funcionHash(valor)
		if @tabla_hash[posicion]== nil
			puts"#{valor} no encontrado"
			return
		end
		if @tabla_hash[posicion].value == valor
			puts "#{valor} encontrado"
			return @tabla_hash[posicion]
		else
			actual = @tabla_hash[posicion]
			until actual == nil
				if actual.value == valor
					puts "#{valor} encontrado"
					return actual
				end
				actual = actual.next
			end
			puts "#{valor} no encontrado"
			return
		end
	end

	def eliminar(value)
		posicion = funcionHash(value)
		if @tabla_hash[posicion] == nil
			puts "#{value} no encontrado"
			return
		end
		if @tabla_hash[posicion].value == value and @tabla_hash[posicion].next == nil 
			@tabla_hash[posicion] = nil
			puts "#{value} fue eliminado"
			return
		else
			if @tabla_hash[posicion].value == value
				@tabla_hash[posicion] = @tabla_hash[posicion].next
				puts "#{value} fue eliminado"
				return
			end
			actual = @tabla_hash[posicion]
			until actual.next == nil
				if actual.next.value == value
					actual.next = actual.next.next
					puts "#{value} fue eliminado"
				end
				actual = actual.next
			end
			puts "#{valor} no encontrado"
			return
		end
	end

	def dibujar
		posicion = 0
		while posicion != 49
			if @tabla_hash[posicion] != nil
				puts @tabla_hash[posicion].value
				if @tabla_hash[posicion].next != nil
					aux = @tabla_hash[posicion]
					while aux.next != nil
						puts "#{posicion}.- #{aux.next.value}"
						aux = aux.next
					end
				end
				posicion = posicion + 1
			else
				posicion = posicion + 1
			end
		end
	end
end

puts"ingrese opcion: "
puts"  1.-Bateria de Prueba"
puts"  2.-prueba propia"
opcion = gets.chomp

if opcion == "1"

	Prueba = HASH.new
	puts "se inserta numeros al azar incluyendo el 0 "
	Prueba.insertar(2)
	Prueba.insertar(12)
	Prueba.insertar(182)
	Prueba.insertar(43)
	Prueba.insertar(33)
	Prueba.insertar(12389412)
	Prueba.insertar(0)
	puts 
	puts "se dibja la tabla actual con las posiciones ocupadas "
	Prueba.dibujar
	puts 
	puts "se prueba el buscador"
	Prueba.buscar(182)
	Prueba.buscar(41)
	Prueba.buscar(55)
	Prueba.buscar(43)
	puts
	puts "se prueba el eliminar buscando, eliminando numeros que no estan y finalmente dibujando la tabla de nuevo"
	Prueba.eliminar(41)
	Prueba.eliminar(2)
	Prueba.eliminar(33)
	Prueba.buscar(2)
	Prueba.buscar(33)
	Prueba.dibujar
end
if opcion == "2"
	chose = "1"
	while chose != "0"
	Prueba = HASH.new
	puts "opciones: "
	puts "1.-insertar"
	puts "2.-eliminar"
	puts "3.-buscar"
	puts "4.-dibujar"
	puts "0.-Exit"
	chose = gets.chomp
	case chose
		when chose == "1" 
			puts"ingrese numero a insertar"
			n = gets.chomp
			Prueba.insertar(n)
		when chose == "2"
			puts"ingrese numero a eliminar"
			n = gets.chomp
			Prueba.eliminar(n)
		when chose == "3"
			puts"ingrese numero buscado"
			n = gets.chomp
			Prueba.buscar(n)
		when chose == "4"		
	end
	end
else
	puts "opcion no valida"
end


